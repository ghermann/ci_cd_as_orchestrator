from datetime import datetime, timedelta

now = datetime.now()

for i in range(0, 5):
    print(now.strftime("%Y-%m-%d"))
    print((now + timedelta(days=7)).strftime("%Y-%m-%d"))
